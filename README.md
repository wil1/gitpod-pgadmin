# gitpod-pgadmin

This project demonstrates running pgAdmin4 in a Gitpod project.

It uses Gitpod environment variables for the login email and password.  You can set these to whatever you want, or use the default/generated values:

* gp env PGADMIN_DEFAULT_EMAIL=pgadmin@gitpod.io
* gp env PGADMIN_DEFAULT_PASSWORD=$(date | md5sum | head -c 32)
